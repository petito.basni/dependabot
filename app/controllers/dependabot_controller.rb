# frozen_string_literal: true

class DependabotController < ApplicationController
  def index
    @projects = Project.not(configuration: nil).order(name: :asc)
  end
end
